<?php
/**
 * src/StockFileNavisionFTPDownloader.php
 *
 * @author mrx <mma@shopall24.com>
 * 
 * @package DataImportStockFileNavisionFTPDownloader
 * @subpackage StockFileNavisionFTPDownloader
 * @version 1.0.0
 */

// Directory Processor
use DataImportDirectoryProcessor\DirectoryProcessor;

// .env
use Dotenv\Dotenv;
// Log
use Monolog\Logger;
use Monolog\Handler\StreamHandler;


// Free spech https://github.com/Behat/Behat/blob/master/bin/behat
if (is_file($autoload = getcwd() . '/vendor/autoload.php')) {
    require $autoload;
}

if (is_file($autoload = __DIR__ . '/../vendor/autoload.php')) {
    require($autoload);
} elseif (is_file($autoload = __DIR__ . '/../../../autoload.php')) {
    require($autoload);
} else {
    fwrite(STDERR,
        'You must set up the project dependencies, run the following commands:'.PHP_EOL.
        'curl -s http://getcomposer.org/installer | php'.PHP_EOL.
        'php composer.phar install'.PHP_EOL
    );
    exit(1);
}

$dotenv = new \Dotenv\Dotenv( dirname( __DIR__ ));
$dotenv->load();

// Log
$log = new Logger('data-import-directory-processor');
$log->pushHandler(new StreamHandler(getenv('PATH_TO_LOG_FILE'), Logger::INFO));
$log->info('Called..');


$dp = new DirectoryProcessor ();


try { 
    $log->info('Processing..');
    fwrite(STDERR,"Processing.. \n");
    $dp->process(
        [
            'directoryToProcess' => getenv('DIRECTORY_TOPROCESS'),
            'directoryProcessing' => getenv('DIRECTORY_PROCESSING'),
            'directoryProcessfails' => getenv('DIRECTORY_PROCESSFAILS'),
            'directoryProcessed' => getenv('DIRECTORY_PROCESSED'),
            'logger' => $log,
            'mongoServer' => getenv('MONGO_SERVER'),
            'mongoPort' => getenv('MONGO_PORT'),
            'mongoDBName' => getenv('MONGO_DB_NAME'),
            'mongoCollectionName' => getenv('MONGO_COLLECTION_NAME'),
            'mongoCollectionSA24Name' => getenv('MONGO_COLLECTION_SA24_NAME'), 
        ]
    );

    $log->info("processed: " . getenv('DIRECTORY_TOPROCESS'));
}catch( Exception $e) {
    // write message to the log file
    $log->error(var_export($e, true));
    fwrite(STDERR,$e->getMessage() . "\n");
}
$log->info("Done.");
