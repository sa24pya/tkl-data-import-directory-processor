# TKL data-import-directory-processor
TKL data-import-directory-processor
TKL Core provides basic library.  
Support PSR-1, PSR-2, PSR-3, PSR-4.

![sa24 package repository](https://www.dream-destination-wedding.com/images/exotic2.jpg)

## Install

Via Composer

``` bash
$ composer require tkl/data-import-directory-processor
```

## Usage

``` bash
$ php
$ 
```

## Testing

``` bash
$ phpunit
```

## Contributing


## Credits

- [Wilson Smith](https://github.com/wilsonsmith)
- [All Contributors](ttps://mma-sa24@bitbucket.org/tkl/tkl-directory-processor.git/contributors)

## License

The GNU General Public Licence version 3 (GPLv3). Please see [License File](LICENSE.md) for more information.
