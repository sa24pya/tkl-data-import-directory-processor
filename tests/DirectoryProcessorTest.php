<?php
/**
 * File: tests/DirectoryProcessorTest.php
 *
 * @author Francesc Roma Frigole <cesc@roma.cat>, mrx <silentstar@riseup.net>
 *
 * @package DataImportDirectoryProcessor
 * @subpackage DirectoryProcessor
 * @version 1.0.0
 *
 */
namespace DataImportDirectoryProcessor;

use DataImportDirectoryProcessor\DirectoryProcessor;
use Core\Tool\ListXMLFiles;
use Core\InputValidate\MissingParameterException;

// .env
use Dotenv\Dotenv;
// Log
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

/**
 * Class DirectoryProcessorTest
 *
 * @package DataImportDirectoryProcessor
 * @subpackage DirectoryProcessor
 * @version 1.0.0
 */
class DirectoryProcessorTest extends \PHPUnit_Framework_TestCase {

    /**
     * App environemnt has impact on congif values
     *
     * @var string
     */
    const APP_ENV = "development";
    public function setUp() 
    {
        $dotenv = new Dotenv(__DIR__);
        $dotenv->load();
    }
    public function tearDown() {}

    /**
     * Call with bad arguments: expet exception
     * @expectedException Core\InputValidate\MissingParameterException
     */
    public function testMissingDirectoryProcessedParameter() {
        $dp = new DirectoryProcessor ();

        $dp->process ( [
                'directoryToProcess' => 'dpDirectoryToProcess',
                'directoryProcessing' => 'dpDirectoryProcessing'
        ] );
    }

    /**
     * Call with bad arguments: expet exception
     * @expectedException Core\InputValidate\MissingParameterException
     */
    public function testMissingDirectoryProcessingdParameter() {
        $dp = new DirectoryProcessor ();

        $dp->process ( [
                'directoryToProcess' => 'dpDirectoryToProcess',
                'directoryProcessed' => 'dpDirectoryProcessed'
        ] );
    }

    /**
     * Call with bad arguments: expet exception
     * @expectedException Core\InputValidate\MissingParameterException
     */
    public function testMissingDirectoryToProcessParameter() {
        $dp = new DirectoryProcessor ();

        $dp->process ( [
                'directoryProcessing' => 'dpDirectoryProcessing',
                'directoryProcessed' => 'dpDirectoryProcessed'
        ] );
    }

    /**
     * Call with bad arguments: expet exception
     * @expectedException Core\InputValidate\NotADirectoryException
     */
    public function testNotADirectory() {
        $dp = new DirectoryProcessor ();
 
        $dp->process ( [
                'directoryToProcess' => 'dpDirectoryToProcess',
                'directoryProcessing' => 'dpDirectoryProcessing',
                'directoryProcessfails' => 'dpDirectoryProcessfails',
                'directoryProcessed' => 'dpDirectoryProcessed'
        ] );
    }
    public function testAllParametersAreOk() {
        // prepare the fixture: make sure that the working folders are curl_multi_remove_handle
        $workingDirectories = [
                getenv('DIRECTORY_TOPROCESS'),
                //getenv('DIRECTORY_PROCESSING'),
                //getenv('DIRECTORY_PROCESSFAILS'),
                //getenv('DIRECTORY_PROCESSED')
        ];

        foreach ( $workingDirectories as $dir ) {
            if(file_exists($dir)){
                $files = scandir ( $dir );
                foreach ( $files as $file ) {
                    if (is_file ( $file )) {
                        $this->assertTrue ( is_writable ( $file ) );
                        $this->assertTrue ( unlink ( $file ) );
                    }
                }
            } else {
                throw new \Exception("No test directories, missing " . getcwd() . $dir, 1);
            }
            
        }

        // now copy the files from the sample directory to the process DirectoryProcessor
        $samples = ListXMLFiles::getList ( getenv('DIRECTORY_XMLSAMPLES') );

        foreach ( $samples as $sample ) {
            $basename = basename ( $sample );
            $dest = getenv('DIRECTORY_TOPROCESS') . '/' . $basename;
            $this->assertTrue ( copy ( $sample, $dest ) );
        }

        // log(arg)
        $logger = new Logger('core-mongo-indexmanipulator');
        $logger->pushHandler(new StreamHandler(getenv('PATH_TO_LOG_FILE'), Logger::INFO));

        $dp = new DirectoryProcessor ();
        $dp->process(
            [
                'directoryToProcess' => getenv('DIRECTORY_TOPROCESS'),
                'directoryProcessing' => getenv('DIRECTORY_PROCESSING'),
                'directoryProcessfails' => getenv('DIRECTORY_PROCESSFAILS'),
                'directoryProcessed' => getenv('DIRECTORY_PROCESSED'),
                'logger' => $logger,
                'mongoServer' => getenv('MONGO_SERVER'),
                'mongoPort' => getenv('MONGO_PORT'),
                'mongoDBName' => getenv('MONGO_DB_NAME'),
                'mongoCollectionName' => getenv('MONGO_COLLECTION_NAME'),
                'mongoCollectionSA24Name' => getenv('MONGO_COLLECTION_SA24_NAME'),
            ]
        );
    }

}
