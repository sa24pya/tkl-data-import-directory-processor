<?php
/**
 * File: DirectoryProcessor.php
 *
 * @author Francesc Roma Frigole <cesc@roma.cat>, mrx <silentstar@riseup.net>, Victor
 *
 * @package DataImportDirectoryProcessor
 * @subpackage DirectoryProcessor
 * @version 1.0.0
 *
 */
namespace DataImportDirectoryProcessor;

// Requires

use Core\Tool\ListXMLFiles;
use Core\Mongo\IndexManipulator;
use Core\InputValidate\InputValidate;

use DataImportXMLCatalogueItemExtractor\XMLCatalogueItemExtractor;
use DataImportProductPersister\ProductPersister;

// use Core\InputValidate\MissingParameterException;
// use Core\InputValidate\NotADirectoryException;


/**
 * Class DirectoryProcessor

 * Process each file in directoryToProcess from XML parsing to
 * database persistence
 *
 * @author Francesc Roma Frigole <cesc@roma.cat>, mrx <silentstar@riseup.net>, Victor
 *
 * @package DataImportDirectoryProcessor
 * @subpackage DirectoryProcessor
 * @version 1.0.0
 */
class DirectoryProcessor
{
    /**
     * @configKeys associative array with configuration options
     */
    private $configKeys = [
        'directoryToProcess',
        'directoryProcessing',
        'directoryProcessed' ,
        'directoryProcessfails'
    ];

    /**
     * process all the files in a given directory
     * we process only files (not dirs) and only with the .
     * xml extension
     * before we process each file we move it to a processing directory and
     * after the file is processed it's moved to the preocessed diretory
     *
     * @param
     *            config array of input pg_parameter_status
     * @param
     *            config[directoryToProcess]
     * @param
     *            config[directoryProcessing]
     * @param
     *            config[directoryProcessed]
     * @param
     *            config[directoryProcessfails]
     *
     * @throws MissingParameterException if one of the required parameters is
     *         Missing
     * @throws NotADirectoryException if one of the required directories directories
     *         not exist
     */
    public function process(array $config = null)
    {

        // Input validation
        InputValidate::config($config, $this->configKeys);

        // directoryToProcess
        $directoryToProcess = $config['directoryToProcess'];
        // Input validation
        InputValidate::directories(array($directoryToProcess));


        // we process only files (not dirs) in directoryToProcess and only
        // those with the .xml extension and move them to the processing
        // directory before starting

        // Files
        $files = \Core\Tool\ListXMLFiles::getList($directoryToProcess);
        if (empty($files)) {
            $message = "No file to process in $directoryToProcess";
            throw new \Exception($message);
        }

        // directoryProcessing
        $directoryProcessing = $config ['directoryProcessing'];
        if (!is_writable(dirname($directoryToProcess))) {
            $message = "Directory $directoryToProcess is not wirteble.";
            throw new \Exception($message);
        }

        echo dirname($directoryToProcess) . DIRECTORY_SEPARATOR . $directoryProcessing;

        if (!file_exists(dirname($directoryToProcess) . DIRECTORY_SEPARATOR . $directoryProcessing)) {
            mkdir(dirname($directoryToProcess) . DIRECTORY_SEPARATOR . $directoryProcessing);
        }

        // directoryProcessed
        $directoryProcessed = $config ['directoryProcessed'];
        if (!is_writable(dirname($directoryToProcess))) {
            $message = "Directory $directoryToProcess is not wirteble.";
            throw new \Exception($message);
        }
        if (!file_exists(dirname($directoryToProcess) . DIRECTORY_SEPARATOR . $directoryProcessed)) {
            mkdir(dirname($directoryToProcess) . DIRECTORY_SEPARATOR . $directoryProcessed);
        }

        // directoryProcessfails
        $directoryProcessfails = $config ['directoryProcessfails'];
        if (!is_writable(dirname($directoryToProcess))) {
            $message = "Directory $directoryToProcess is not wirteble.";
            throw new \Exception($message);
        }
        if (!file_exists(dirname($directoryToProcess) . DIRECTORY_SEPARATOR . $directoryProcessfails)) {
            mkdir(dirname($directoryToProcess) . DIRECTORY_SEPARATOR . $directoryProcessfails);
        }


        // Stats
        $countFiles = count($files);
        $i = 0;

        // Log
        $key = 'logger';
        if (array_key_exists($key, $config)) {
            $logger = $config[$key];
            // write message to the log file
            $logger->info('import started on folder '.$directoryToProcess.' with '.$countFiles.' number of files to process');
        }
        
        

        $temp = 0;
        $idx = new IndexManipulator($logger);
        // We remove the indexes before importing all the files since with them is 15-20 times more slower:
        $idx->dropIndexes($config);
        foreach ($files as $file) {
            $i ++;

            // path/to/file/destination/basename
            $fileDestination = dirname($directoryToProcess)
               . DIRECTORY_SEPARATOR . $directoryProcessing
               . DIRECTORY_SEPARATOR . basename($file) ;

            rename($file, $fileDestination);

            // chain of responsabilities pattern: (ask Matteo :) )
            // the input and output of every element of the chain is stored in the array
            // therefore most values are fuplicated in the array since they are both output
            // of one process and input of the next one

            // first step: extract the XML catalog items from the XML file
            // each catalog item contains the whole hierarchy of pallet - box - consumer item
            // (or one of the variations of it)

            $extractor = new XMLCatalogueItemExtractor();
            $results ['fileToProcess'] = $fileDestination;
            $results ['XMLCatalogueItemExtractor'] = $extractor->extract($results);
            if (!isset($results ['XMLCatalogueItemExtractor'])) {
                $temp++;
                $fileDestinationOld = $fileDestination ;
                $fileDestination = dirname($directoryToProcess)
                    . DIRECTORY_SEPARATOR . $directoryProcessfails
                    . DIRECTORY_SEPARATOR . basename($file);

                rename($fileDestinationOld, $fileDestination);
            }
            // the second step converts the XML into an array of catalog items in array format
            // i.e. into an array of associative arrays
            // $transformer = new CatalogueItem2ArrayTransformer;
            // $results[ 'arrayToProcess' ] = $results[ 'XMLCatalogueiItemextractor' ];
            // $results[ 'CatalogueItem2ArrayTransformer' ] = $transformer->transform( $results );

            // the third and last element of the chain will persist in the databae each one of this
            // catalog items as an independent doucument
            $persister = new ProductPersister();
            $config ['arrayToProcess'] = $results ['XMLCatalogueItemExtractor'];
            $config ['fileToProcess'] = $fileDestination ;
            $results ['persister'] = $persister->persist($config);

            if (!$results ['persister']) {
                $temp++;
                $fileDestinationOld = $fileDestination ;
                $fileDestination = dirname($directoryToProcess)
                    . DIRECTORY_SEPARATOR . $directoryProcessfails
                    . DIRECTORY_SEPARATOR . basename($file);

                rename($fileDestinationOld, $fileDestination);
            } else {
                // after each file has been processsed we move it to directoryProcessed
                // path/to/file/destination/basename
                $fileDestinationOld = $fileDestination ;
                $fileDestination = dirname($directoryToProcess)
                    . DIRECTORY_SEPARATOR . $directoryProcessed
                    . DIRECTORY_SEPARATOR . basename($file);

                rename($fileDestinationOld, $fileDestination);
            }
        }
        // And then we add them before exiting:
        $idx->createIndexes($config);

        // Log
        $key = 'logger';
        if (array_key_exists($key, $config)) {
            $logger = $config[$key];
            // write message to the log file
            $logger->info('import completed on folder '.$directoryProcessed.' with '.($countFiles - $temp).' files successfully imported '.$temp.' files skipped');
        }
    }
}
